import psycopg2


create_table_cadeau = """
                        CREATE TABLE IF NOT EXISTS cadeau(
                            id serial PRIMARY KEY,
                            libelle varchar(50)
                        )
                        """

create_table_cadeau_lettre = """
                                CREATE TABLE IF NOT EXISTS cadeau_lettre(
                                    id_cadeau integer REFERENCES cadeau (id),
                                    id_lettre integer REFERENCES lettre (id),
                                    CONSTRAINT PK_ca_lt PRIMARY KEY (id_cadeau,id_lettre)
                                )
                            """

create_table_lettre = """
                        CREATE TABLE IF NOT EXISTS lettre(
                            id INTEGER PRIMARY KEY,
                            code_pays char(3) REFERENCES pays (code),
                            departement varchar(30),
                            sage bool,
                            corps_lettre text
                        )
                        """

create_table_enfant = """
                        CREATE TABLE IF NOT EXISTS enfant(
                            id SERIAL PRIMARY KEY,
                            nom varchar(30),
                            age integer,
                            id_lettre integer REFERENCES lettre (id)
                        )
                    """

create_table_pays = """
                        CREATE TABLE IF NOT EXISTS pays(
                            code char(3) PRIMARY KEY,
                            nom varchar(32)
                        )
                    """

def connexion():
    try:
        conn = psycopg2.connect("dbname='santa_data' user='santa' host='localhost' password='claus'")
    except:
        print("I am unable to connect to the database")
        
    return conn


def rquery(conn, requete):
    cur = conn.cursor()
    cur.execute(requete)

    retour = cur.fetchall()

    return retour


def query(conn, requete):
    cur = conn.cursor()
    cur.execute(requete)



if __name__ == "__main__":
    with connexion() as conn:
        """query(conn,"DROP TABLE enfant;")
        query(conn,"DROP TABLE cadeau_lettre;")
        query(conn,"DROP TABLE lettre;")
        query(conn,"DROP TABLE pays;")"""


        query(conn,create_table_cadeau)
        query(conn,create_table_pays)
        query(conn,create_table_lettre)
        query(conn,create_table_enfant)
        query(conn,create_table_cadeau_lettre)

        conn.commit()


        retour = rquery(conn,"SELECT COUNT(*) FROM cadeau_lettre")

        print(retour)
