import creation_tables as ct
import re
from difflib import get_close_matches

path_lettres = r"Partie_1/lettres_pere_noel/"
pre_file = "lettre_"
suf_file = ".pnoel"

query_pays = """
                SELECT code FROM pays WHERE nom = %s;
            """

ins_cadeau = """
                INSERT INTO cadeau (libelle)
                VALUES (%s)
            """

ins_lettre = """
                INSERT INTO lettre (id, code_pays, departement, sage, corps_lettre) 
                VALUES (%s, %s, %s, %s, %s);
            """
ins_enfant = """
                INSERT INTO enfant (nom, age, id_lettre)
                VALUES (%s, %s, %s)
            """
ins_cadeau_lettre = """
                        INSERT INTO cadeau_lettre (id_cadeau, id_lettre)
                        VALUES (%s, %s)
                    """

def getAges(age2):

    age = sep_ages.split(age2)

    age_def = []
    ismois = False
    if age.count("mois") > 0:
        ismois = True
    for val in age:
        if has_num.match(val) and not val == "1/2" and not val == "3/4":
            valdef = ""
            for c in val:
                
                if c.isdigit():
                    valdef += c
                elif c == "/" and  len(valdef)>1 and (int(val[0])>1):
                    valdef = valdef[0:-1]
                    break
                elif len(valdef) >0:
                    break
            if ismois and valdef:
                valdef = str(int(valdef) // 12)
            age_def.append(valdef)
    return age_def


def get_dom_tom(dom_tom,pays):
    departement = ""
    if pays.capitalize() in dom_tom:
        departement = pays
        pays = "FRANCE"
    else:
        departproche = get_close_matches(pays.capitalize(),dom_tom)
        if(len(departproche) > 0):
            departement = departproche[0].upper()
            pays = "FRANCE"
    return pays, departement


def get_pays(cur,pays2,liste):
    
    pays = pays2.capitalize()
    cur.execute(query_pays,(pays,))
    val_pays = cur.fetchall()
    if len(val_pays) < 1:
        nearpays = get_close_matches(pays,liste)
        if len(nearpays) >0:
            pays = nearpays[0]
        else:
            pays = None
       
    else:
        pays = val_pays[0][0]
    return pays

if __name__ == "__main__":

    str_sage = re.compile(".+ sage.+")
    ls_key_ask = ["je voudrais","avoir","apportes"]
    sep_excl_noms = re.compile(".*(classe|école).*",flags=re.IGNORECASE)
    sep_noms = re.compile("/| ou | et | & | |\.")
    sep_ages = re.compile(" et | & | ")'          ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────                    '
    has_num = re.compile(".*\d.*")
    pas_sage = re.compile(".+pas( \w*)+ sage.+",flags=re.UNICODE)

    dom_tom = ["Saint-Pierre-et-Miquelon", "Saint-Barthélemy", "Saint-Martin", "Wallis-et-Futuna", "Polynésie française", "Guadeloupe", "Martinique", "Guyane française", "Réunion", "Mayotte"]

    islettre = re.compile("\w|-",flags=re.UNICODE)

    with ct.connexion() as conn:
        cur = conn.cursor()

        lpays = ct.rquery(conn,"SELECT nom FROM pays;")[0]

        for num_file in range(1,1641):
            nom_fichier = path_lettres + pre_file + str(num_file) + suf_file

            with open(nom_fichier,"r",encoding="latin1") as fichier:
                lignes = fichier.readlines()

                #   ---    corps de la lettre
                lettre = lignes[3].strip()
                #détecter cadeau

                #détecter sage
                is_sage = None
                if str_sage.match(lettre):
                    #print(lettre)
                    is_sage = True
                    if pas_sage.match(lettre):
                        #print("nope")
                        is_sage = False



                #   --- pays
                pays = lignes[2].strip().upper()
                
                loc = get_dom_tom(dom_tom,pays)
                pays = loc[0]
                departement = loc[1]
                
                
                if not has_num.match(pays):
                    pays = get_pays(cur,pays,lpays)
                    
                

                try:
                    id_lettre = str(int(lignes[4].strip())) # ai-je des cas non int ? apparemment non mais dans le doute
                except ValueError as e:
                    print(e)



                #   --- enfant
                nom = lignes[0].strip().capitalize()
                #split, retrait mot ou caractère de liaison
                if sep_excl_noms.match(nom):
                    continue

                nom = sep_noms.split(nom)
                nomdef = []
                for prenom in nom:
                    buff = ""
                    for c in prenom:
                        if islettre.match(c):
                            buff +=  c

                    if len(buff)>1:
                        nomdef.append(buff)

                #print(nomdef)
                
                age = lignes[1].strip()
                #split, détecter et supprimer demi
                age_def = getAges(age)
                if len(age_def)<1:
                    if not pays == None and len(pays) > 0:
                        age_def = getAges(pays)
                        if len(pays) != 3:
                            pays = None
                
                print(lignes[4].strip())

                
                #creer cadeau

                # creer lettre
                cur.execute(ins_lettre,(id_lettre,pays,departement,is_sage,lettre))

                #creer enfant
                if len(nomdef) > 0:
                    if len(age_def) == len(nomdef):
                        for ienf in range(0,len(nomdef)):
                            cur.execute(ins_enfant,(nomdef[ienf],age_def[ienf],id_lettre))
                    else:
                        if len(age_def) > 0:
                            for ienf in range(0,len(nomdef)):
                                cur.execute(ins_enfant,(nomdef[ienf],age_def[0],id_lettre))
                        else:
                            for ienf in range(0,len(nomdef)):
                                cur.execute(ins_enfant,(nomdef[ienf],None,id_lettre))


                conn.commit()
                #creer cadeau-lettre



   # with creat.connexion() as conn:
